function reverseWords(string){
    let wordsArr = string.split(' ');
    let reverseWordsArr = [];

    wordsArr.forEach(word => {
        let wordArr = word.split('');
        let reverseWord = wordArr.reverse();
        reverseWordsArr.push(reverseWord.join(''))
    });

    return reverseWordsArr.join(' ');
}

console.log(reverseWords('Coding Javascript'));
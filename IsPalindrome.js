function isPalindrome(string) {
    string = string.toLowerCase();
    let charactersArr = string.split('');
    let validCharacters = 'abcdefghijklmnopqrstuvwxyz'.split('');

    let lettersArr = [];
    charactersArr.forEach(char => {
        if (validCharacters.indexOf(char) > -1) lettersArr.push(char);
    });

    return lettersArr.join('') === lettersArr.reverse().join('');
}

console.log(isPalindrome("Madam, I'm Adam"));